package com.example.demo.service;

import com.example.demo.model.ShoppingCard;

import java.util.List;

public interface ShoppingCardService {
    List<ShoppingCard> findByUser(String user);

    void deleteAll();

    void deleteAllByUser(String user);

    void addToShoppingCard(String user, String item, int currentCount, int selectCount, int price);

    boolean buyProductForUser(String nameUser);
}
