package com.example.demo;

import com.example.demo.repository.ShoppingCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ScheduledTasks {
    @Autowired
    ShoppingCardRepository shoppingCardRepository;

    @Autowired
    @Qualifier("sessionRegistry")
    private SessionRegistry sessionRegistry;

    @Scheduled(fixedDelay = 60000) // check every 1 min
    public void deleteOldOrders(){
        List<UserDetails> principals = sessionRegistry.getAllPrincipals()
                .stream()
                .filter(principal -> principal instanceof UserDetails)
                .map(UserDetails.class::cast)
                .collect(Collectors.toList());
        if (!principals.isEmpty()) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, -10); // 10 min
            Date tenMinAgo = calendar.getTime();
            for (UserDetails user : principals) {
                if (!user.getUsername().isEmpty()) {
                    if (!shoppingCardRepository.findAllByUser(user.getUsername()).isEmpty()) {
                        shoppingCardRepository.deleteAllByUser(user.getUsername());
                    }
                }
            }
        }
    }
}
