package com.example.demo.repository;

import com.example.demo.model.ShoppingCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface ShoppingCardRepository extends JpaRepository<ShoppingCard, Long> {
    @Override
    Optional<ShoppingCard> findById(Long aLong);

    @Override
    void deleteById(Long aLong);

    @Override
    void deleteAll();

    @Override
    <S extends ShoppingCard> S save(S entity);

    @Transactional
    @Modifying
    @Query("select u from ShoppingCard u where  u.user= :user")
    List<ShoppingCard> findAllByUser(String user);

    @Transactional
    @Modifying
    @Query("DELETE FROM ShoppingCard e WHERE e.user = :user")
    void deleteAllByUser(String user);

    @Transactional
    @Modifying
    @Query("select u from ShoppingCard u where  u.user= :user and u.item = :item")
    List<ShoppingCard> findByUserAndItem(String user, String item);

    @Transactional
    @Modifying
    @Query("update ShoppingCard e set e.count = :count, e.summ = :summ where e.user = :user and e.item = :item")
    void updateProduct(@Param("count") int count, @Param("summ") int summ, @Param("user") String user, @Param("item") String item);
}
