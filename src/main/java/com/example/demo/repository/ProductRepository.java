package com.example.demo.repository;

import com.example.demo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findById(Long id);

    Optional<Product> findByDescription(String desc);

    @Override
    void deleteById(Long aLong);

    @Transactional
    @Modifying
    @Query("update Product e set e.description = :description, e.count = :count, e.price = :price where e.id = :id")
    void updateProduct(@Param("id") Long productId, @Param("description") String description, @Param("count") int count, @Param("price") int price);

    @Override
    <S extends Product> S save(S entity);


    @Transactional
    @Modifying
    @Query("update Product u set u.count = :count where u.description = :description")
    void updateProduct4Buying(@Param("description") String description, @Param("count") int count);
}
