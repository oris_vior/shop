package com.example.demo.service;

import com.example.demo.dto.ProductDto;
import com.example.demo.model.Product;

import java.util.List;

public interface ProductService {
    Product findById(Long id);
    Product createProduct(ProductDto productDto);
    List<Product> findAllProduct();

    void delete(Long id);

    void updateProduct(Long id, ProductDto productDto);

    ProductDto createProductDto(String description, String count, String price);

    void updateProduct4User(String item, int changeCount);
}
