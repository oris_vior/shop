package com.example.demo.controllers;

import com.example.demo.service.AuthenticationService;
import com.example.demo.service.ShoppingCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class UserController {

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    ShoppingCardService shoppingCardService;

    @PostMapping("/shoppingCardPage")
    public String shoppingCardPage(Model model) {
        model.addAttribute("userName", authenticationService.getNameUser());
        model.addAttribute("shoppingCard", shoppingCardService.findByUser(authenticationService.getNameUser()));
        return "card";
    }

    @PostMapping("/shoppingCard")
    public String getShoppingCard(@RequestParam("description") String description,
                                  @RequestParam("currentCount") int currentCount,
                                  @RequestParam("price") int price,
                                  @RequestParam("selectCount") int selectCount
    ) {

        shoppingCardService.addToShoppingCard(authenticationService.getNameUser(), description, currentCount, selectCount, price);
        return "redirect:/";
    }

    @PostMapping("/deleteAllFromCard")
    public String deleteAllFromCard() {
        shoppingCardService.deleteAllByUser(authenticationService.getNameUser());
        return "redirect:/";
    }

    @PostMapping("/buyProduct")
    public String buyProduct(RedirectAttributes redirectAttributes) {
        boolean checkMessageAboutBuying =  shoppingCardService.buyProductForUser(authenticationService.getNameUser());
        redirectAttributes.addAttribute("checkMessageAboutBuying", checkMessageAboutBuying);
        return "redirect:/";
    }
}
