package com.example.demo.service;

import com.example.demo.dto.ProductDto;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

@Service
public class ValidatorService {
    @SneakyThrows
    public void validationProductDto(ProductDto productDto) {
        if (productDto.getDescription().isEmpty()) {
            throw new Exception("Description must be filled in");
        }
        if (productDto.getPrice() > 1000 || productDto.getPrice() < 0) {
            throw new Exception("Price must be more than 0 and less than 1000");
        }
        if (productDto.getCount() < 0 || productDto.getCount() > 50) {
            throw new Exception("Count must be more than 0 and less than 50");
        }
    }
}
