package com.example.demo.service;

import com.example.demo.dto.ProductDto;
import com.example.demo.model.Product;
import com.example.demo.repository.ProductRepository;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ValidatorService validatorService;

    @SneakyThrows
    @Override
    public Product findById(Long id) {
        if (productRepository.findById(id).isPresent()) {
            return productRepository.findById(id).get();
        }
        throw new Exception("not found item");
    }

    @Override
    public Product createProduct(ProductDto productDto) {
        return productRepository.save(convertFronDtoToProduct(productDto));
    }

    @Override
    public List<Product> findAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void updateProduct(Long id, ProductDto productDto) {
        productRepository.updateProduct(id, productDto.getDescription(), productDto.getCount(), productDto.getPrice());
    }

    private Product convertFronDtoToProduct(ProductDto productDto) {
        return new Product(productDto.getDescription(), productDto.getCount(), productDto.getPrice());
    }

    public ProductDto createProductDto(String description, String count, String price) {
        ProductDto productDto = new ProductDto();
        productDto.setDescription(description);
        productDto.setCount(Integer.parseInt(count));
        productDto.setPrice(Integer.parseInt(price));
        validatorService.validationProductDto(productDto);
        return productDto;
    }

    @SneakyThrows
    @Override
    public void updateProduct4User(String item, int changeCount) {
        Optional<Product> product = productRepository.findByDescription(item);
        if (product.isPresent()) {
            productRepository.updateProduct4Buying(product.get().getDescription(), product.get().getCount() - changeCount);
        } else {
            throw new Exception("Not found;");
        }
    }
}
