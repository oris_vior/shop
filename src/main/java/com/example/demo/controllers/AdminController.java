package com.example.demo.controllers;

import com.example.demo.dto.ProductDto;
import com.example.demo.model.Product;
import com.example.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminController {

    @Autowired
    ProductService productService;

    @PostMapping("/addNewProduct")
    public String addNewProduct(@RequestParam ("description") String description,
                                 @RequestParam("count") String count,
                                 @RequestParam("price") String price) {
        ProductDto productDto = productService.createProductDto(description, count, price);
        productService.createProduct(productDto);
        return "redirect:/";
    }

    @GetMapping("/newProductPage")
    public String addNewProduct(){
        return "new_product";
    }

    @PostMapping("/updateProductPage")
    public String updateProductPage(@RequestParam("id") String id,
                                    @RequestParam("description") String decs,
                                    @RequestParam("count") String count,
                                    @RequestParam("price") String price
    ) {
        productService.updateProduct(Long.valueOf(id), productService.createProductDto(decs, count, price));
         return "redirect:/";
    }

    @PostMapping("/updateProduct")
    public String updateProduct(@RequestParam("id") String id, Model model){
        Product product = productService.findById(Long.valueOf(id));
        model.addAttribute("id", id);
        model.addAttribute("desc", product.getDescription());
        model.addAttribute("count", product.getCount());
        model.addAttribute("price", product.getPrice());
        return "update_product";
    }
}
