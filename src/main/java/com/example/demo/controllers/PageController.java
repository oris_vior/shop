package com.example.demo.controllers;

import com.example.demo.service.AuthenticationService;
import com.example.demo.service.ProductService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class PageController {

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    ProductService productService;

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/admin")
    public String test1() {
        return "test";
    }

    @GetMapping("/")
    public String defaultPage(Model model, @RequestParam(value = "checkMessageAboutBuying", required = false) Boolean checkMessageAboutBuying){
        if (checkMessageAboutBuying != null) {
            model.addAttribute("messageCheck", checkMessageAboutBuying);
        }
        model.addAttribute("isAdmin", authenticationService.checkUser());
        model.addAttribute("name", authenticationService.getNameUser());
        model.addAttribute("listProduct", productService.findAllProduct());
        return "default";
    }

    @PostMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        request.logout();
        return "redirect:/login?logout";
    }
}
