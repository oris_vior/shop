package com.example.demo.service;

import com.example.demo.model.Order;
import com.example.demo.model.ShoppingCard;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.ShoppingCardRepository;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingCardServiceImpl implements ShoppingCardService {

    @Autowired
    ShoppingCardRepository shoppingCardRepository;

    @Autowired
    ProductService productService;

    @Autowired
    OrderService orderService;

    @Override
    public List<ShoppingCard> findByUser(String user) {
        return shoppingCardRepository.findAllByUser(user);
    }

    @Override
    public void deleteAll() {
        shoppingCardRepository.deleteAll();
    }

    @Override
    public void deleteAllByUser(String user) {
        shoppingCardRepository.deleteAllByUser(user);
    }

    @SneakyThrows
    @Override
    public void addToShoppingCard(String user, String item, int currentCount, int selectCount, int price) {
        if (shoppingCardRepository.findByUserAndItem(user, item).isEmpty()) {
            ShoppingCard shoppingCard = new ShoppingCard();
            shoppingCard.setUser(user);
            shoppingCard.setItem(item);
            shoppingCard.setCount(selectCount);
            shoppingCard.setSumm(price * selectCount);
            shoppingCardRepository.save(shoppingCard);
        }
        else {
            ShoppingCard shoppingCard = shoppingCardRepository.findByUserAndItem(user, item).get(0);
            if (selectCount + shoppingCard.getCount() < currentCount) {
                shoppingCardRepository.updateProduct(shoppingCard.getCount() + selectCount, (price * selectCount) + shoppingCard.getSumm(), user, item);
            }
            else {
                throw new Exception("current count < select count");
            }
        }
    }

    @SneakyThrows
    @Override
    public boolean buyProductForUser(String nameUser) {
        if (!findByUser(nameUser).isEmpty()) {
            for (ShoppingCard shoppingCard : findByUser(nameUser)) {
                productService.updateProduct4User(shoppingCard.getItem(), shoppingCard.getCount());
                orderService.saveOrder(new Order(nameUser, shoppingCard.getItem(), shoppingCard.getCount(), shoppingCard.getSumm()));
            }
            shoppingCardRepository.deleteAllByUser(nameUser);
            return true;
        } else {
            throw new Exception("Card is Empty!");
        }
    }
}
